# ftp://postel.obs-mip.fr/Vegetation/LAI/CYCLOPES/Data/V3.1/3B_WORLD_1KM/Bio/2001/H29V11/
# ftp://login:19870814passwordt28synxk@postel.obs-mip.fr/Vegetation/LAI/CYCLOPES/Data/V3.1/3B_WORLD_1KM/Bio/2001/H29V11/CYCL_BIO_1km_V3.1_VGT_H29V11_2001005.hdf.gz
source("load.R")

url <- paste0("ftp://19870814:t28synxk",
              "@postel.obs-mip.fr/Vegetation/LAI/CYCLOPES/",
              "Data/V3.1/3B_WORLD_1KM/Bio/2001/H29V11/",
              "CYCL_BIO_1km_V3.1_VGT_H29V11_2001005.hdf.gz")

writeName <- file.path("downloads","CYCLOPES","data","2001005.hdf.gz")

curl_download_withtest(writeName, url)

gunzip(writeName)
file.name <- "downloads/CYCLOPES/data/2001005.hdf"

sds <- get_subdatasets(file.name)
# Isolate the name of the first sds
name <- sds[1]
filename <- 'downloads/CYCLOPES/data/2001005.tif'
gdal_translate(sds[1], dst_dataset = filename)
# Load the Geotiff created into R
r <- raster(filename)

# grid.num <- sprintf("H%sV%s",29,11)
# grid.num.all <-  sprintf("H%sV%s",29:33,10:12)
grid.num.all <- c()

for (i in 29:33){
  for (j in 10:12){
    
    grid.num.all[length(grid.num.all)+1] <- sprintf("H%sV%s",i,j)
  }
}

remove <- c("H29V10","H33V10")

grid.num.all <- grid.num.all[!grid.num.all %in% remove]

grid.num.all[length(grid.num.all)+1] <- "H32V13"

######

down.cyclopes.func <- function(year,grid.num,...){
  base.url <- paste0("ftp://19870814:t28synxk",
                     "@postel.obs-mip.fr/Vegetation/LAI/CYCLOPES/",
                     "Data/V3.1/3B_WORLD_1KM/Bio")
  
  
  # year <- 2001
  
  day <- c(seq(5,25,10),
           seq(36,46,10),
           seq(64,84,10),
           seq(95,145,10),
           seq(156,206,10),
           seq(217,237,10),
           seq(248,298,10),
           seq(309,359,10))
  
  if (year == 2004){
    day <- c(seq(5,25,10),
             seq(36,46,10),
             seq(65,85,10),
             seq(96,146,10),
             seq(157,207,10),
             seq(218,238,10),
             seq(249,299,10),
             seq(310,360,10))
  }
  
  
  fix.fn <- "CYCL_BIO_1km_V3.1_VGT_"
  
  gz.fn <- paste0(fix.fn,grid.num,"_",year,sprintf("%003d.hdf.gz",day))
  
  url <- file.path(base.url,year,grid.num,gz.fn)
  
  for (i in 1:length(url)){
    writeName <- file.path("downloads","CYCLOPES","data",gz.fn[[i]])
    # options(timeout = 3600*20)
    curl_download_withtest(writeName, url[[i]],...)
  }
  
}

# options(timeout=190) 
# for (i in seq_along(grid.num.all)){
#   down.cyclopes.func(2001,grid.num.all[i],handle = new_handle(timeout=190))
# }

for (yr in 2001:2007){
  
  for (i in seq_along(grid.num.all)){
    try(down.cyclopes.func(yr,grid.num.all[i],handle = new_handle(timeout=190)))
  }
}

# unzip files
gz.fn <- list.files(file.path("downloads","CYCLOPES","data"))
gz.fn <- gz.fn[grep("[.]gz",gz.fn)]

for(i in 1:length(gz.fn)){
  
  out.fn <- substr(gz.fn[[i]][1],1,(nchar(gz.fn[i][1])-3))
  
  gunzip(file.path("downloads","CYCLOPES","data",gz.fn[i]),
         destname=file.path("downloads","CYCLOPES","data","hdf",out.fn),
         remove = FALSE)
  
}

# transorm to tif 
hdf.fn <- list.files(file.path("downloads","CYCLOPES","data","hdf"))

gdal_chooseInstallation(hasDrivers=c("HDF4","HDF5"))
# Get the version of this installation:
getOption("gdalUtils_gdalPath")[[
  gdal_chooseInstallation(hasDrivers=c("HDF4","HDF5"))]]$version

for(i in 1:length(hdf.fn)){
  # i=1
  filename <- paste0(substr(hdf.fn[[i]][1],1,(nchar(hdf.fn[i][1])-4)),".tif")
  in.fn <- file.path("downloads","CYCLOPES","data","hdf",hdf.fn[i])
  out.fn <- file.path("downloads","CYCLOPES","data","tif",filename)
  gdal_translate(in.fn, dst_dataset = out.fn,sd_index=1)
}

gdalinfo(in.fn)


# read tif as rasters

for (j in 1:length(grid.num.all)){
  # j=1
  tif.path <- file.path("downloads","CYCLOPES","data","tif")
  
  file.list <- list.files(tif.path,pattern = c(grid.num.all[j]))
  
  file.list <- file.list[grep("[.]tif",file.list)]
  
  raster.ls <- list()
  
  for (i in 1:length(file.list)){
    
    temp.ls <- raster(file.path(tif.path,file.list[i]))
    
    raster.ls[[i]] <- as.matrix(temp.ls)
    # these two values report the best quality data
    
    raster.ls[[i]][raster.ls[[i]] > 181] <- NA
  }
  
  cyc_array <- abind(raster.ls, along=3)
  
  cyc_annual <- rowMeans(cyc_array,na.rm = TRUE,dims=2)
  
  annual.mean.fn <- file.path(tif.path,paste0(grid.num.all[j],".rds"))
  
  saveRDS(cyc_annual,file=annual.mean.fn)
}


# plot()
# projection(raster(cyc_annual))

tif.path <- file.path("downloads","CYCLOPES","data","tif")

# rds.list <- list.files(tif.path,pattern = "[.]rds")

# fill the sea with NAs
temp.m <- matrix(NA,ncol = 1120,nrow = 1120)

na.r.all <- c("H29V10","H33V10","H29V13","H30V13","H31V13","H33V13")

for (i in 1:length(na.r.all)){
  
  tif.path <- file.path("downloads","CYCLOPES","data","tif")
  saveRDS(temp.m,file=file.path(tif.path,paste0(na.r.all[i],".rds")))
  
}
# put the pieces together.
m.part.ls <- list()
cells.vec <- 29:33
rds.list <- list.files(tif.path,pattern = "[.]rds")
for (j in 1: length(cells.vec)){
  
  temp.fn <- rds.list[grep(paste0(cells.vec[j]),rds.list)]
  
  temp.ls <- list()
  for (i in 1:length(temp.fn)){
    tif.path <- file.path("downloads","CYCLOPES","data","tif")
    fn.path <- file.path(tif.path,paste0(temp.fn[i]))
    temp.ls[[i]] <- readRDS(fn.path)
  }
  m.part.ls[[j]] <- do.call(rbind,temp.ls)
}

m.all <- do.call(cbind,m.part.ls)

m.t <- t(m.all/30)
# plot(raster(t(m.t)))

# reproject to climate
# and get the data to align with our outputs

long.cyc <- seq(110,160,length.out = nrow(m.t))
lati.cyc <- seq(-10,-50,length.out = ncol(m.t))
lati <- ncvar_get(nc_open("downloads/pavg/pavg2002.nc"), "latitude")
long <- ncvar_get(nc_open("downloads/pavg/pavg2002.nc"), "longitude")

col.lati <- c()
for (i in 1:length(lati)){
  col.lati[i] <- which(abs(lati.cyc-lati[i]) == min(abs(lati.cyc-lati[i])))
}
row.long <- c()
for (i in 1:length(long)){
  row.long[i] <- which(abs(long.cyc-long[i])==min(abs(long.cyc-long[i])))
}

m.sub.cyc <- m.t[row.long,col.lati]
m.sub.cyc[3300:4110,0:450] <- NA
m.sub.cyc[0:1500,0:450] <- NA
# plot(raster(t(m.sub.cyc)))

saveRDS(m.sub.cyc,"output/data/cyc/cyc.01.07.rds")

