# The default setting is to down load ten years ot data from 2001 to 2011. 
# But you can change is by setting the yearB and yearE
sim_years <- 1991:2011

# PPT and arad are annual and are thus processed together:
# vpd is done alone; so is Tmax. Both of them are monthly.

# get PPT and arad####
var.nm <-  "radiation"

rad.url <- paste0("http://dapds00.nci.org.au/thredds/fileServer/rr9/eMAST-R-Package/eMAST-R-Package_v1-1_",
                  var.nm,
                  "-mean_yearly_0-01deg_1970-2012/00000000/eMAST-R-Package_v1-1_",
                  var.nm,
                  "-mean_yearly_0-01deg_1970-2012_00000000_",
                  sim_years,
                  ".nc")

get.met(sim_years,var.nm,"annual_mean_radiation",url=rad.url)
# 
var.nm <-  "rainfall"

map.url <- paste0("http://dapds00.nci.org.au/thredds/fileServer/rr9/eMAST-R-Package/eMAST-R-Package_v1-1_",
                  var.nm,
                  "_yearly_0-01deg_1970-2012/00000000/eMAST-R-Package_v1-1_",
                  var.nm,
                  "_yearly_0-01deg_1970-2012_00000000_",
                  sim_years,
                  ".nc")
get.met(sim_years,var.nm,"lwe_thickness_of_precipitation_amount",url=map.url)

# get tmax####

t.url <- paste0("http://dapds00.nci.org.au/thredds/fileServer/rr9/ANUClimate/ANUClimate_v1-0_",
                "temperature-max_monthly_0-01deg_1970-2014/00000000/ANUClimate_v1-0_",
                "temperature-max_monthly_0-01deg_1970-2014_00000000_")

get.t(sim_years  = sim_years,"temperature",baseUrl = t.url,var.long.nm = "air_temperature")

# get pet####
pet.url <- paste0("http://dapds00.nci.org.au/thredds/fileServer/rr9/eMAST-R-Package/eMAST-R-Package_v1-1_",
                "potential-evapotranspiration_monthly_0-01deg_1970-2012/00000000/eMAST-R-Package_v1-1_",
                "potential-evapotranspiration_monthly_0-01deg_1970-2012_00000000_")

get.t(sim_years  = sim_years,"pet",baseUrl = pet.url,var.long.nm = "potential_evapotranspiration")

# get annual vpd ####
get.vpd(years=sim_years)

#20 year average climate####
get20mean("temperature")
get20mean("rainfall")
get20mean("vpd")
get20mean("pet")

get20mean("radiation")

# radiation unit is wrong is the original data set
# and the coord is off too
rad.m <- readRDS("cache/radiation.rds")
saveRDS((rad.m * 365/2)[,3:3476],"cache/par.rds")






