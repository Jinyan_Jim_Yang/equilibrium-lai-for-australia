# note:
# data are only about trees. 
# data are not annual mean but are just one of two days.

# read tern data
tern.annual <- read.csv("downloads/TERN/summary/all tern data.csv",header = TRUE)
tern.annual$date <- strptime(as.character(tern.annual$date),"%d/%m/%Y")
# replace cumberland lai with face data
# face LAI
library(HIEv)
# HIEv R package will download files to here:
setToPath("downloads")
facelai <- downloadCSV("FACE_P0037_RA_GAPFRACLAI_20121026-20150322_L2.csv")
facelai$Date <- strptime(as.character(facelai$Date),"%Y-%m-%d")

dt.face <- data.frame(1,"EucFACE","NSW",facelai$Date,
                      (150+44/60),
                      -(33+37/60),
                      facelai$LAI,"Remko Duursma 2016")

names(dt.face) <- names(tern.annual)
tern.sub <- tern.annual[which(!tern.annual$Name == "Cumberland"),]

tern.annual <- rbind(tern.sub,dt.face)
# get.modis.for.coord.func((150+44/60),-(33+37/60))
# coord for vpd
lati <- ncvar_get(nc_open("downloads/rainfall/rainfall2002.nc"), "latitude")
long <- ncvar_get(nc_open("downloads/rainfall/rainfall2002.nc"), "longitude")

# find closest value
num.lati <- c()
for (i in 1:length(tern.annual$Latitude)){
  num.lati[i] <- which(abs(lati-tern.annual$Latitude[i])==min(abs(lati-tern.annual$Latitude[i])))
}

num.longi <- c()
for (i in 1:length(tern.annual$Longitude)){
  num.longi[i] <- which(abs(long-tern.annual$Longitude[i])==min(abs(long-tern.annual$Longitude[i])))
}

# simulated tern
#use 20 year mean climate data
vpd.tern<-c()
VPD.20.mean <- readRDS(file.path("cache","vpd.rds"))
for (i in 1:length(num.longi)){
  vpd.tern[i] <- VPD.20.mean[num.longi[i],num.lati[i]]
}
par.tern<-c()
par.20.mean <- readRDS(file.path("cache","par.rds"))
for (i in 1:length(num.longi)){
  par.tern[i] <- par.20.mean[num.longi[i],num.lati[i]]
}
tmax.tern <- c()
tmax.20.mean <- readRDS(file.path("cache","temperature.rds"))
for (i in 1:length(num.longi)){
  tmax.tern[i] <- tmax.20.mean[num.longi[i],num.lati[i]]
}

rain.tern <- c()
rain.20.mean <- readRDS(file.path("cache","rainfall.rds"))
for (i in 1:length(num.longi)){
  rain.tern[i] <- rain.20.mean[num.longi[i],num.lati[i]]
}

result.tern <- lai_ppt.func(vpd = vpd.tern,
                            rain.test = rain.tern,
                            par.vec = par.tern,
                            tmax =tmax.tern)

result.tern$measured <- tern.annual$LAI
result.tern$site <- tern.annual$Name

result.tern$lon <- tern.annual$Longitude
result.tern$lat <- tern.annual$Latitude
saveRDS(result.tern,file = "output/data/result.tern.rds")

