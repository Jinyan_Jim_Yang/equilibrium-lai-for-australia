
# note about iio's data:
# first data is filetered -- lai max is selevted when possible
# then the whole system LAI is used instead of just trees
# link for iio dataset
iio.url <- "http://onlinelibrary.wiley.com/store/10.1111/geb.12133/asset/supinfo/geb12133-sup-0001-as1.xls?v=1&s=1cbb9e88028e39fd231fe4e03dc21b92e9ec34aa"

curl_download(iio.url,"downloads/iio.xls",quiet = FALSE)

wb <-  loadWorkbook("downloads/iio.xls")
iio.all <-  readWorksheet(wb, sheet = "Appendix_S1_Dataset", header = TRUE)
iio.data <- iio.all[-seq(1,6),]

names(iio.data) <- iio.all[6,]
# load iio data
iio.aust <- iio.data[grep("Australia",iio.data$Country),]
iio.aust <- iio.aust[grep("Natural",iio.aust$Vegetation_status),]
iio.aust$Latitude <- as.numeric(iio.aust$Latitude)
iio.aust$Longitude <- as.numeric(iio.aust$Longitude)
iio.aust$Total_LAI <- as.numeric(iio.aust$Total_LAI)
iio.aust$`MAP_(Literature value)` <- as.numeric(iio.aust$`MAP_(Literature value)`)

######################################################################
# coord for vpd
lati <- ncvar_get(nc_open("downloads/rainfall/rainfall2002.nc"), "latitude")
long <- ncvar_get(nc_open("downloads/rainfall/rainfall2002.nc"), "longitude")

# get cordination
coord.s <- iio.aust$Latitude
coord.w <- iio.aust$Longitude



# find closest value
num.lati <- c()
for (i in 1:length(coord.s)){
  num.lati[i] <- which(abs(lati-coord.s[i])==min(abs(lati-coord.s[i])))
}

num.longi <- c()
for (i in 1:length(coord.s)){
  num.longi[i] <- which(abs(long-coord.w[i])==min(abs(long-coord.w[i])))
}

# simulated iio
#use 20 year mean climate data
vpd.iio<-c()
VPD.20.mean <- readRDS(file.path("cache","vpd.rds"))
for (i in 1:length(num.longi)){
  vpd.iio[i] <- VPD.20.mean[num.longi[i],num.lati[i]]
}
par.iio<-c()
par.20.mean <- readRDS(file.path("cache","par.rds"))
for (i in 1:length(num.longi)){
  par.iio[i] <- par.20.mean[num.longi[i],num.lati[i]]
}
tmax.iio <- c()
tmax.20.mean <- readRDS(file.path("cache","temperature.rds"))
for (i in 1:length(num.longi)){
  tmax.iio[i] <- tmax.20.mean[num.longi[i],num.lati[i]]
}

rain.iio <- c()
rain.20.mean <- readRDS(file.path("cache","rainfall.rds"))
for (i in 1:length(num.longi)){
  rain.iio[i] <- rain.20.mean[num.longi[i],num.lati[i]]
}


result.iio <- lai_ppt.func(vpd = vpd.iio,
                           rain.test = rain.iio,
                           par.vec = par.iio,
                           tmax =tmax.iio)

# make data set
result.iio$measured <- iio.aust$Total_LAI
result.iio$site <- iio.aust$Reference_number
result.iio$lon <- iio.aust$Longitude
result.iio$lat <- iio.aust$Latitude

saveRDS(result.iio,file = "output/data/result.iio.rds")
