source("load.R")
#ggregate the 20 year mean#####################################################################
VPD.20.mean.m <- readRDS(file.path("cache","vpd.rds"))
VPD.20.mean.m <- as.matrix(aggregate(x=raster(VPD.20.mean.m),fact=6,FUN=mean,na.rm=TRUE,expand=TRUE))

rain.20.mean <- readRDS(file.path("cache","rainfall.rds"))
rain.20.mean[which(rain.20.mean < 0)] <- NA
rain.20.mean <- as.matrix(aggregate(x=raster(rain.20.mean),fact=6,FUN=mean,na.rm=TRUE,expand=TRUE))

tmax.20.mean <- readRDS(file.path("cache","temperature.rds"))
tmax.20.mean <- as.matrix(aggregate(x=raster(tmax.20.mean),fact=6,FUN=mean,na.rm=TRUE,expand=TRUE))
                          
par.20.mean <- readRDS(file.path("cache","par.rds"))
par.20.mean <- as.matrix(aggregate(x=raster(par.20.mean),fact=6,FUN=mean,na.rm=TRUE,expand=TRUE))

pet.m <-  readRDS("cache/pet.rds")
pet.de.m <- as.matrix(aggregate(x=raster(pet.m),fact=6,FUN=mean,na.rm=TRUE,expand=TRUE))

# save aggreagated data with only 2 digit after decimal
saveRDS(round(VPD.20.mean.m,2),"cache/vpd.sub.de.rds")
saveRDS(round(rain.20.mean,2),"cache/map.sub.de.rds")
saveRDS(round(tmax.20.mean,2),"cache/tmax.sub.de.rds")
saveRDS(round(par.20.mean,2),"cache/par.sub.de.rds")
saveRDS(round(pet.de.m,2),"cache/pet.sub.de.rds")

vpd.de.m <- (readRDS(file.path("cache","vpd.sub.de.rds")))
map.de.m <- readRDS(file.path("cache","map.sub.de.rds"))
map.de.m[which(map.de.m < 0)] <- NA
tmax.de.m <- readRDS(file.path("cache","tmax.sub.de.rds"))
par.de.m <- readRDS(file.path("cache","par.sub.de.rds"))

#model####
temp.vec <- mapply(g1.lai.e.func,
                   VPD = vpd.de.m,
                   E = map.de.m,
                   PAR = par.de.m, 
                   TMAX = tmax.de.m)


# output file names
file.Name <- c("g1", "LAI","Gs","NCG","LUE","GPP")
# save result
out.path <- file.path("output","data")
for(i in 1:length(file.Name)){
  var.vec <- temp.vec[2,]
  var.m <- matrix(var.vec,
                  ncol = ncol(tmax.20.mean),
                  nrow = nrow(tmax.20.mean))
  fn <- file.path(out.path, sprintf("%s1991_2011.rds",file.Name[i]))
  saveRDS(var.m,file.path(fn))
}

# Required processes to get the figures and all
# make sure all the needed data and packages are there before you go ahead

# file.nm <- list.files("r/",pattern = "process_")
# for(i in file.nm){
#   source(paste0("r/",i))
# }
