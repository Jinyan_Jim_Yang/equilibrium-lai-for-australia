---
output:
  pdf_document:
    fig_caption: yes
    fig_height: 10
    fig_width: 10
  word_document: default
---
```{r, echo=FALSE,warning=FALSE,message=FALSE}
par(cex = 1.4)
library(raster)
  dir.path <- dirname(getwd())
  # 
  map.de.m <- readRDS(file.path(dir.path,"cache/map.sub.de.rds"))
vpd.de.m <- readRDS(file.path(dir.path,"cache/vpd.sub.de.rds"))
par.de.m <- readRDS(file.path(dir.path,"cache/par.sub.de.rds"))
tmax.de.m <- readRDS(file.path(dir.path,"cache/tmax.sub.de.rds"))
pet.m <- readRDS(file.path(dir.path,"cache/pet.sub.de.rds")) * 12
lai.m <- readRDS(file.path(dir.path,"output/data/LAI1991_2011.rds"))
modis.m <- readRDS(file.path(dir.path,"cache/modis.sub.rds"))
lcm <- readRDS(file.path(dir.path,"output/data/lcm.rds"))
lai.full <- lai.m
modis.full <- modis.m


diff.full <- lai.full - modis.full
diff.full.nt <- diff.full[280:400,30:150]
lai.m.NT.sel <- lai.m[280:400,30:150]
modis.m.NT.sel <- modis.m[280:400,30:150]
lc.sub.m.NT <- lcm[280:400,30:150]
map.m.NT.sel <- map.de.m[280:400,30:150]
vpd.m.NT.sel <- vpd.de.m[280:400,30:150]
pet.m.NT.sel <- pet.m[280:400,30:150] 
diff <- (lai.m - modis.m)
diff.nt <- diff[280:400,30:150]

nt.df <- data.frame(lai = as.vector(lai.m.NT.sel),
                    modis = as.vector(modis.m.NT.sel),
                    map = as.vector(map.m.NT.sel),
                    vpd = as.vector(vpd.m.NT.sel),
                    pet = as.vector(pet.m.NT.sel))
nt.df$AI <- nt.df$pet / nt.df$map
  # 
  LAI.model <- lai.m
  lai.array <- as.vector(LAI.model)
  modis.sub <- modis.m
  lcm <- readRDS(file.path(dir.path,"output/data/lcm.rds"))
  modis.array <- as.vector(modis.sub)
  modis.de.vec <- as.vector(modis.sub)
  LAI.model.o <-  LAI.model
  lai.array.o <- as.vector(LAI.model.o)
  lai.array <- as.vector(LAI.model.o)

  # get bias and rmse
rmse.func <- function(error){
  sqrt(sum(error^2,na.rm = 1)/length(na.omit(error)))
}

plot.contour.func <- function(cyc.sub.de.vec,lai.array,plot.nm,y.nm){
  
  test.df <- data.frame(lai.cyc = cyc.sub.de.vec,
                        lai.opt = lai.array,
                        num = 1)
  
  
  see.m <- with(test.df, tapply(num, 
                                list(
                                  lai.cyc=cut(lai.cyc, breaks=seq(0,5,0.05), include.lowest=T), 
                                  lai.opt=cut(lai.opt, breaks=seq(0,5,0.05), include.lowest=T)
                                ),
                                sum)
  )
  
  see.m <- see.m[nrow(see.m):1,]
  
  par(mar=c(5,5,2,7))

  b2y.fun <- colorRampPalette(c("grey80","darkseagreen","darkgreen","orange","coral"))
  
  brks.diff <- seq(0,5,length.out = 6)
  arg <- list(at=brks.diff, labels=paste0(10^(brks.diff)))
  plot(raster(log10(see.m),
              xmn=0, xmx=5, ymn=0, ymx=5),
       asp = 1,
       breaks=brks.diff,
       lab.breaks = brks.diff,
       col=c(b2y.fun(length(brks.diff) - 1)),
       axis.args = arg,
       legend.args=list(text="Count", side=3, font=2, line=0.5, cex=1),
       xlab=expression(italic(L[opt])),
       ylab=y.nm,
       cex=1.2)
  
  
  legend("topleft",legend = c("1:1","Regression"),
         col = "black",lty=c("solid","dashed"),cex=1,bty = "n")
  
  
  fit.opt.cyc <- lm(cyc.sub.de.vec~lai.array)

  bias.c.o <- mean(lai.array - cyc.sub.de.vec,na.rm = 1)
  rmse.c.o <- rmse.func(lai.array - cyc.sub.de.vec)
  
  mylabel.b <- bquote(Bias == .(format(bias.c.o, digits = 2)))
  mylabel.r <- bquote(RMSE == .(format(rmse.c.o, digits = 2)))
  text(x = 4, y = 1, labels = mylabel.b)
  text(x = 4, y = 0.5, labels = mylabel.r)

  adj.r.sqrt <- summary(fit.opt.cyc)$r.squared
  mylabel <- bquote(italic(R)^2 == .(format(adj.r.sqrt, digits = 2)))
  text(x = 4, y = 3, labels = mylabel)
  abline(a=0,b=1)
  abline(fit.opt.cyc,lty="dashed")
  title(plot.nm,adj=0)
}

```

---
```{r, echo=FALSE,warning=FALSE,message=FALSE}

  par(mfrow=c(2,2),
      mar=c(5,5,5,5)) 

 # plot a 
  par(mar=c(1,5,5,1))
  brks <- c(0,700)
  raster::plot(raster::raster(t(lcm)),
  breaks=brks,col=c("grey80"),
       legend=FALSE,ann=FALSE,axes=FALSE, box=FALSE, bty="n")

  brks.diff <- c(-4,-1,-0.5,0,0.5,1,4)
arg <- list(at=brks.diff, labels=paste0(brks.diff))


b2r.fun<- colorRampPalette(c('orange','orange2', 'red3','darkred'))



b2y.fun <- colorRampPalette(rev(c('lightskyblue','steelblue3','royalblue3', 'darkblue')))

  diff.raster <- LAI.model.o - modis.sub
 
    raster::plot(raster::raster(t(diff.raster)),
                                # xmn=112.905, xmx=153.995, ymn=-43.375, ymx=-9.005),
                 xlab="lon",ylab="lat",
                 # xlim=c(0,1),ylim=c(0,1),
                 breaks=brks.diff,
                 col=c(b2y.fun(3),
                       b2r.fun(3)),
                 ann=FALSE,
                 axes=FALSE,
                 add = TRUE, 
                 box=FALSE,
                 legend = FALSE,
                 bty="n")
    
    plot(raster::raster(t(diff.raster)), 
         legend.only=TRUE,
         breaks=brks.diff,
         col=c(b2y.fun(3),b2r.fun(3)),
                 lab.breaks=brks.diff,axis.args = arg,
                 legend.width=1,
                 legend.shrink=1.1,
                 legend.args=list(text=expression(m^2~m^-2), side=3, font=2, line=3, cex=1),
         smallplot=c(0.04,0.09, 0.25,0.7))
  title(expression("(a)."~italic(L[opt])~"-"~MODIS),adj=0)
  
  
plot.contour.func(cyc.sub.de.vec = modis.de.vec,
                  lai.array = lai.array,
                  plot.nm = expression("(b)"),
                  y.nm = expression("MODIS"))
                  

# c
 brks.diff <- c(-4,-1,-0.5,0,0.5,1,4)
  arg <- list(at=brks.diff, labels=paste0(brks.diff))

plot(raster(t(diff.nt)),
             breaks=brks.diff,
             col=c(b2y.fun(3),
                       b2r.fun(3)),
             lab.breaks=brks.diff,
             ann=FALSE,axes=FALSE,
             axis.args = arg,box=FALSE, 
             bty="n",
             legend.args=list(text=expression(m^2~m^-2), side=3, font=2, line=1.5, cex=1))
title(expression((c)~"NT:"~italic(L[opt])~"-"~MODIS),cex.main = 1,font = 1,line = 1,adj=0)


# d
par(mar=c(5,7,3,2))
brks <- c(0.5,1.5,2.5,3.5,4.5)
col.func <- colorRampPalette(c("navy","red"))
palette(col.func(length(brks)+1))
AI.cut <- cut(nt.df$AI,breaks = brks)
with(nt.df,plot((lai - modis)/modis~modis,
                xlab=expression("MODIS"~(m^2~m^-2)),
                ylab=expression(italic("(L"[opt])*" - MODIS) / MODIS"),
                ylim=c(-0.5,3),
                xlim=c(0,3),
                pch=16,cex=0.6,col=AI.cut))
legend("topright",levels(AI.cut),fill=palette(),title = "AI",bty='n')
title(expression("(d)"),line=1,adj=0)
abline(h=0,lty="dashed")

```

Figure 4