This repo contents the code for Yang et al. 2017 JAMES "Applying the concept of ecohydrological equilibrium to predict steady-state leaf area index". 

verison 1.0.

The required packages are listed in the load.r file. R 3.3 is recomended to run the code.

Feel free to contact Jinyan Yang (Jim) via email (jyyoung at live.com) or leave a comment.

